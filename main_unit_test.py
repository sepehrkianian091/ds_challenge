import unittest

from challenge import Challenge
from disk_manager import DiskManager

# For Testing Functionalities
test_1 = [
    # For Creating User
    "0 1 10001 user_1 00100",  # Create User user_1
    "0 1 10002 user_2 00100",  # Create User user_2
    "1 1 10001",  # Online Friends user_1 (expected: [])
    # For Follow/Unfollow 2Cases
    "0 4 10001 10002 0",  # user_1 unfollows user_2? (Nothing Changes. even Cost. cause we have loaded user_1 before)
    "0 4 10001 10002 1",  # user_1 follows user_2 (Cost Doesn't Change, Cause we have loaded user_1 before
    # For Changing Status
    "1 1 10001",  # Online Friends user_1 (expected: [10002])
    "0 2 10002 0 10000",  # user_2 is offline now
    "1 1 10001",  # Online Friends user_1 (expected: []),
    "0 2 10002 1 00000",  # user_2 is online now
    "1 1 10001",  # Online Friends user_1 (expected: [10002])
    # For Blocking/Unblocking and following on those cases
    "0 2 10001 1 00000",  # user_1 is online now
    "0 3 10001 10002 0",  # user_1 unblocks user_2 (Nothing Changes, Even Costs)
    "0 4 10002 10001 1",  # user_2 follows user_1
    "1 1 10002",  # online friends user_2 (expected: [10001])
    "0 3 10001 10002 1",  # user_1 blocks user_2
    "1 1 10001",  # online_friends user_1 (expected: [])
    "1 1 10002",  # online_friends user_2 (expected: [])
    "0 4 10001 10002 1",  # user_1 follows user_2
    "0 4 10002 10001 1",  # user_2 follows user_1
    "0 3 10002 10001 1",  # user_2 blocks user_1
    "0 4 10001 10002 1",  # user_1 follows user_2
    "0 4 10002 10001 1",  # user_2 follows user_1
    "0 3 10001 10002 0",  # user_1 unblocks user_2
    "0 4 10001 10002 1",  # user_1 follows user_2
    "0 4 10002 10001 1",  # user_2 follows user_1
    "1 1 10001",  # online_friends user_1 (expected: [])
    "1 1 10002",  # online_friends user_2 (expected: [])
    "0 3 10002 10001 0",  # user_2 unblocks user_1
    "1 1 10001",  # online_friends user_1 (expected: [])
    "1 1 10002",  # online_friends user_2 (expected: [])
    "0 4 10001 10002 1",  # user_1 follows user_2
    "0 4 10002 10001 1",  # user_2 follows user_1
    "1 1 10001",  # online_friends user_1 (expected: [10002])
    "1 1 10002",  # online_friends user_2 (expected: [10001])
    # For Testing Follow/Unfollow
    "0 4 10001 10002 0",  # user_1 unfollows user_2
    "1 1 10001",  # online_friends user_1 (expected: [])
    "1 1 10002",  # online_friends user_2 (expected: [10001])
    "0 4 10002 10001 1",  # user_2 follows user_1
    "1 1 10001",  # online_friends user_1 (expected: [])
    "1 1 10002",  # online_friends user_2 (expected: [10001])
    "0 4 10002 10001 0",  # 2 unfollows 1
    "0 4 10001 10002 1",  # 1 follows 2
    "1 1 10001",  # online_friends user_1 (expected: [10002])
    "1 1 10002",  # online_friends user_2 (expected: [])
    # For FollowSuggestions Query
    "0 4 10002 957 1",  # user_2 follows 957
    "0 4 10002 237 1",  # user_2 follows 237
    "0 4 10002 51 1",  # user_2 follows 51
    "0 4 10002 122 1",  # user_2 follows 122
    "1 2 10001 10000000",  # Follow Suggestions for user_1 (expected: [???])
    "1 2 10002 10000",  # Follow Suggestions for user_2 (expected: [???])
    "1 2 10002 10000000",  # Follow Suggestions for user_2 (expected Something Totally Different Than before: [???])
]

outs_1 = [
    # For Creating User
    '',
    # For Follow/Unfollow 2Cases
    # For Changing Status
    '10002',
    '',
    '10002',
    # For Blocking/Unblocking and following on those cases
    '10001',
    '',
    '',
    '',
    '',
    '',
    '',
    '10002',
    '10001',
    # For Testing Follow/Unfollow
    '',
    '10001',
    '',
    '10001',
    '10002',
    '',
    # For FollowSuggestions Query
    '51 122 957 237',
    '9877 2729 6351 5037 8956 2764 5308 6003 3892 7046 4039 3094 7595 6980 1785 8410 8219 8508 740 9696',
    '8542 3916 2837 7225 5576 7339 7794 6117 2786 4109 44 1355 2938 8680 851 521 2309 1850 2539 965'
]

# For Testing Block Loads
test_2 = [
    "0 4 1 101 1",                      # 1 Follows 101
    "0 4 201 301 1",                    # 201 Follows 301
    "0 4 401 501 1",                    # 401 Follows 501
    "0 4 601 701 1",                    # 601 Follows 701
    "0 4 801 901 1",                    # 801 Follows 901
    "0 4 1001 1101 1",                  # 1001 Follows 1101
    "0 4 1 101 1",                      # 1 Follows 101
    "0 4 201 301 1",                    # 201 Follows 301
    "0 4 401 501 1",                    # 401 Follows 501
    "0 4 601 701 1",                    # 601 Follows 701
    "0 4 801 901 1",                    # 801 Follows 901
    "0 4 1001 1101 1",                  # 1001 Follows 1101
]
outs_2 = [
]

test_main = [
    "1 1 210",
    "0 1 10001 hamed 1618497229",
    "0 2 21 1 1618497229",
    "1 2 210 1618497229",
    "0 4 210 3618 0",
    "0 4 210 3618 1",
    "0 3 210 3618 1"
]
outs_main = [
    '3618 1485 5932 1287 2791 4284 9880 406 7432 8135 785 6162 5617 7768 6287 4877 8455 2713 1855 6604 6138 2836 6258 3241 8989 7123 7809 5886 3019 2047 6848 4987 5881 6246 7474 7882 6538 8619 8392 1575 2544 8801 2588 5905 7378 8686 5895 7208 7766 6284 5966 4517 5138 8318 7247 7414 8127 6013 715 7770 8520 5647 9521',
    '809 8100 8740 6820 790 288 1584 6518 6447 7063 6224 7736 6454 5854 9481 1119 649 8546 1819 2233'
]


class MainUnitTest(unittest.TestCase):
    def test_whole_process(self):
        self.func_of_test(test=test_1, outs_test=outs_1)

    def test_block_loads(self):
        self.func_of_test(test=test_2, outs_test=outs_2)

    def test_using_main(self):
        self.func_of_test(test=test_main, outs_test=outs_main)

    def func_of_test(self, test: list, outs_test: list):
        dm = DiskManager()
        app = Challenge(dm)
        outs = app.run(test)
        self.assertEqual(outs, outs_test)
