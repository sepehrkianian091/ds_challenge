from User import User
from cache import Cache
import os
import psutil


class Challenge:
    def __init__(self, disk_manager):
        self.cache = Cache(disk_manager=disk_manager)
        self.ev_handler = {
            1: lambda event: self.create_user(user_id=event[0], name=event[1], last_seen=event[2]),
            2: lambda event: self.change_status(user_id=event[0], inst_type=int(event[1]), last_seen=event[2]),
            3: lambda event: self.block_or_unblock(id_1=event[0], id_2=event[1], inst_type=int(event[2])),
            4: lambda event: self.change_follow(id_1=event[0], id_2=event[1], inst_type=int(event[2]))
        }

        self.query_handler = {
            1: lambda event: self.find_online_friends(user_id=event[0]),
            2: lambda event: self.find_follow_suggestions(user_id=event[0], suggestion_time=event[1])
        }

    def run(self, events_and_queries):
        outs = []

        ev_or_query_handler = {
            0: lambda event: self.ev_handler.get(int(event[1]))(event[2:]),
            1: lambda query: self.query_handler.get(int(query[1]))(query[2:])
        }
        for index, ev_or_query in enumerate(events_and_queries):
            split = ev_or_query.split(" ")
            out = ev_or_query_handler.get(int(split[0]))(split)
            if out is not None:
                outs.append(out)

        process = psutil.Process(os.getpid())
        print('program_size_occupation:', process.memory_info().rss)  # in bytes
        return outs

    # Create an empty user and insert it into the disk
    def create_user(self, user_id, name, last_seen):
        user = User(user_id=user_id, name=name, epoch_last_seen=last_seen)
        self.cache.insert_user(user=user)

    # Get User, Change Its Status, and insert it into the disk
    def change_status(self, user_id, inst_type, last_seen):
        user = self.cache.get_user(user_id=user_id)
        user.status = inst_type == 1
        # If User Is gonna be offline, update its last_seen
        if inst_type == 0:
            user.last_seen = last_seen
        self.cache.put_user_on_db(user=user, replace=True)
        # self.cache.set_online(user_id=user_id, user_status=inst_type)

    # id1 -> id2
    # id1 will block, or unblock id2
    def block_or_unblock(self, id_1, id_2, inst_type):
        user = self.cache.get_user(user_id=id_1)

        # User_1 Unblocks User_2
        if inst_type == 0 and id_2 in user.blocking:
            user.remove_from_list('blocking', id_2)
            self.cache.put_user_on_db(user=user, replace=True)
            # user.blocking.remove(id_2)
        # User_1 Blocks User_2
        elif inst_type == 1 and id_2 not in user.blocking:
            # Add User_2 to User_1 blocking
            # If User_1 follows User_2, Unfollow it
            # If User_1 has follow_suggestion on User_2, Remove It
            user.blocking.append(id_2)

            user.remove_from_list('followings', id_2)
            # user.followings.remove(id_2)
            user.remove_from_list('follow_suggestions', id_2)
            # user.follow_suggestions.remove(id_2)
            self.cache.put_user_on_db(user=user, replace=True)

            # If User_2 follows User_1, Unfollow it
            # If User_2 has follow_suggestion on User_1, Remove It
            user_2 = self.cache.get_user(user_id=id_2)
            user_2.remove_from_list('followings', id_1)
            # user_2.followings.remove(id_1)
            user_2.remove_from_list('follow_suggestions', id_1)
            # user_2.follow_suggestions.remove(id_1)
            self.cache.put_user_on_db(user=user_2, replace=True)

    def change_follow(self, id_1, id_2, inst_type):
        user = self.cache.get_user(user_id=id_1)
        # id_1 Follows id_2 (Only If The Two Haven't Blocked Each Other)
        if inst_type == 1 and id_2 not in user.followings:
            user_2 = self.cache.get_user(user_id=id_2)
            if id_2 not in user.blocking and id_1 not in user_2.blocking:
                user.followings.append(id_2)
                # id_2 will be removed from user's follow_suggestions, too
                user.remove_from_list('follow_suggestions', id_2)
                # user.follow_suggestions.remove(id_2)
                self.cache.put_user_on_db(user=user, replace=True)
        # id_1 UnFollows id_2
        elif inst_type == 0 and id_2 in user.followings:
            user.remove_from_list('followings', id_2)
            # user.followings.remove(id_2)
            self.cache.put_user_on_db(user=user, replace=True)

    # Finding Online Friends.
    # Cache will handle online Users,
    # and for that we'll find out online friends,
    # and return it
    def find_online_friends(self, user_id):
        user = self.cache.get_user(user_id=user_id)
        online_friends = [friend_id for friend_id in user.followings if self.cache.is_online(user_id=int(friend_id))]
        out = ' '.join(online_friends)
        print(out)
        return out

    ten_days_epoch = 864000
    follow_suggestions_num = 20

    # If Last Suggestion is more than 10 days, remove the whole suggestion list and create new
    # else, Add new Suggestions to be 20 on the list
    # Finding Suggestion is done in cache. it has except, and length
    # Finally, Update Last Suggestion Time, Suggestions, and The User, And Print Suggestions_list
    def find_follow_suggestions(self, user_id, suggestion_time):
        suggestion_time = int(suggestion_time)
        user = self.cache.get_user(user_id=user_id)
        # Followings, And Blockings, And Follow Suggestions, Don't Have an intersection. Use Disjoint Set?
        user_except_suggestions = user.follow_suggestions + user.followings + user.blocking
        if suggestion_time - user.follow_suggestions_epoch >= self.ten_days_epoch:
            user.follow_suggestions = self.cache.find_follow_suggestions(user=user, length=self.follow_suggestions_num,
                                                                         except_suggestions=user_except_suggestions)
        else:
            user.follow_suggestions += self.cache.find_follow_suggestions(user=user,
                                                                          length=self.follow_suggestions_num - len(
                                                                              user.follow_suggestions),
                                                                          except_suggestions=user_except_suggestions)
        user.follow_suggestions_epoch = suggestion_time
        self.cache.put_user_on_db(user=user, replace=True)
        out = ' '.join(user.follow_suggestions)
        print(out)
        return out
