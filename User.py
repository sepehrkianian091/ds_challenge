from datetime import datetime


# String: ID Name Number-Of-Friends Friend-ID-1 Friend-ID-2 … Friend-ID-N Status Last-Seen 0

# User -> id, name, friends, status, epoch_last_seen
class User:
    def __init__(self, user_id: str, name: str, epoch_last_seen: str, followings=[], blocking=[],
                 follow_suggestions=[], follow_suggestions_epoch=0, status='online') -> None:
        self.id = user_id
        self.name = name
        self.followings = followings
        self.blocking = blocking
        self.follow_suggestions = follow_suggestions
        self.follow_suggestions_epoch = int(follow_suggestions_epoch)
        self.status = True if status == 'online' else False
        self.last_seen = float(epoch_last_seen)

    def remove_from_list(self, list_name, el):
        if list_name in ['followings', 'blocking', 'follow_suggestions']:
            the_list = self.__getattribute__(list_name)
            if el in the_list:
                the_list.remove(el)

    def __str__(self) -> str:
        strg = "{} {} {} {} {} {} {} {}" \
            .format(self.id, self.name, self.get_separated_string(self.followings), self.status, self.last_seen,
                    self.get_separated_string(self.blocking), self.get_separated_string(self.follow_suggestions),
                    self.follow_suggestions_epoch)
        user_called = User.get_user(user_string=strg)
        return strg

    @staticmethod
    def get_separated_string(the_list: list):
        return str(len(the_list)) + ((' ' + ' '.join(the_list)) if len(the_list) > 0 else '')

    def __lt__(self, other):
        return len(self.followings) < (len(other.followings) if type(other) is User else other)

    def __eq__(self, other):
        return self.id == other.id if type(other) is User else self.id == other

    @staticmethod
    def get_user(user_string: str):
        user_string = user_string.split(" ")
        user_id = user_string[0]
        name = user_string[1]
        friends_length = int(user_string[2])
        friends = user_string[3: 3 + friends_length]
        status = user_string[3 + friends_length]
        epoch_last_seen = user_string[4 + friends_length]
        blocking = []
        follow_suggestions = []
        follow_suggestions_epoch = 0
        if len(user_string) > 5 + friends_length:
            blocking_length = int(user_string[5 + friends_length])
            blocking = user_string[6 + friends_length: 6 + friends_length + blocking_length]
            if len(user_string) > 7 + friends_length + blocking_length:
                follow_suggestions_length = int(user_string[6 + friends_length + blocking_length])
                follow_suggestions = user_string[
                                     7 + friends_length + blocking_length: 7 + friends_length + blocking_length + follow_suggestions_length]
                follow_suggestions_epoch = user_string[7 + friends_length + blocking_length + follow_suggestions_length]
        return User(user_id=user_id, name=name, followings=friends, blocking=blocking,
                    follow_suggestions=follow_suggestions, follow_suggestions_epoch=follow_suggestions_epoch,
                    status=status, epoch_last_seen=epoch_last_seen)
