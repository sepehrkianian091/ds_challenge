import collections

from FollowingHeap import FollowingHeap
from User import User
from disk_manager import DiskManager


class Cache:
    # block limit for Cache -> 3
    # line size limit for Blocks -> 1499
    blocks_length = 3
    block_size = 100
    line_size = 1499
    disk_name = 'dataset'

    def __init__(self, disk_manager: DiskManager) -> None:
        self.disk_manager = disk_manager
        self.disk_size = 0
        self.following_heap = FollowingHeap()
        self.onlines_block = [0]
        self.following_lengths_block = [0]
        self.loaded_blocks = {}
        self.loaded_blocks_lrus = collections.deque()
        self.init_blocks()

    def init_blocks(self):
        # Get The First Block, and Add Every User To The Following Heap, And Set Online Hashes
        # And Remember To Always Increase the disk_size
        block = Cache.to_users(self.disk_manager.read_block(self.disk_name))
        self.disk_size += len(block)
        for user in block:
            # We Use This Instead
            self.put_user_on_db(user=user, replace=False)
            # self.following_heap.add_element(user)
            # self.set_online(user_id=user.id, user_status=user.status)
            # self.set_following_length(user_id=user.id, user_following_length=len(user.followings))

        # Now, Get The Other Blocks,
        # Set Online For Users,
        # if there was one element bigger than min element Of following Heap,
        # Extract Min and put the element in the following Heap,
        # And Remember To Always Increase the disk_size
        block = Cache.to_users(self.disk_manager.read_block(self.disk_name))
        self.disk_size += len(block)
        while len(block) != 0:
            for user in block:
                # We Use This Instead
                self.put_user_on_db(user=user, replace=False)
                # self.set_online(user_id=user.id, user_status=user.status)
                # self.set_following_length(user_id=user.id, user_following_length=len(user.followings))
                # if len(user.followings) > self.following_heap.get_min_following():
                #     self.following_heap.extract_root()
                #     self.following_heap.add_element(user)
            block = Cache.to_users(self.disk_manager.read_block(self.disk_name))
            self.disk_size += len(block)
        pass

    def insert_user(self, user: User):
        # id will be sent here auto_incrementally. So, We know That user.id = self.disk_size + 1
        # So, We Move The Cursor To That Place
        needed_seek = self.get_delta_cursor(self.disk_size)
        self.disk_manager.disk_seek(disk_name=self.disk_name, delta=needed_seek)

        # And Write Through That Cursor. We Can use Write Block,
        # Cause We're Sure That This Is The End Line Of The Disk,
        # And if we use write block, there will be one line inserted at the end of the dis
        self.disk_manager.write_block(disk_name=self.disk_name, data=[user.__str__()])
        self.disk_size += 1

        # We Also Put User On The Status List
        # We Use This Function Instead
        self.put_user_on_db(user=user, replace=False)
        # self.set_online(user_id=user.id, user_status=user.status)
        # self.set_following_length(user_id=user.id, user_following_length=len(user.followings))
        # todo put User On The heap?

    # If The User Was In The Following Heap, get It From There. If Not, Get It From The Blocks
    def get_user(self, user_id: str) -> User:
        if self.following_heap.__contains__(item_id=user_id):
            return self.following_heap.__getitem__(item_id=user_id)
        else:
            return self.get_block(block_id=self.to_block_id(user_id=user_id))[self.to_block_index(user_id=user_id)]

    def put_user_on_db(self, user: User, replace: bool):
        # If User's Following Is Enough To Be On The Heap, We Put It There, And Extract The Root
        self.set_online(user_id=int(user.id), user_status=user.status)
        self.set_following_length(user_id=int(user.id), user_following_length=len(user.followings))
        if self.following_heap.length() < self.block_size:
            self.following_heap.add_element(user)
        elif user.id not in self.following_heap and len(user.followings) > self.following_heap.get_min_following():
            if replace:
                self.put_user_on_db(self.following_heap.extract_root(), replace=True)
            else:
                self.following_heap.extract_root()
            self.following_heap.add_element(user)
        # Else If the User is On The heap, We Replace Its Values
        elif replace and self.following_heap.__contains__(user.id):
            # replace the user on the heap
            self.following_heap.replace_node(node=user)
        # Else, We Just Replace The User's Block
        elif replace:
            loaded_block = self.get_block(block_id=self.to_block_id(user_id=user.id))
            loaded_block[self.to_block_index(user_id=user.id)] = user
            pass

    # There Are Lost Of Friends Of Friends.
    # We Put Those Friends Of Friends Based On Popularity.
    def find_follow_suggestions(self, user: User, length: int, except_suggestions: list) -> list:
        # First We Iterate Through Followings Of The User That Are On The Heap (The Popular Ones)
        gen = [user_id for user_id in user.followings if self.following_heap.__contains__(user_id)]
        user_getter = lambda user_id: self.following_heap.__getitem__(user_id)
        follow_suggestions = self.iterate_for_suggestions(gen=gen, except_suggestions=except_suggestions, length=length,
                                                          user_getter=user_getter)
        # If It wasn't Enough, We Iterate Through Those That Aren't On The Heap (The UnPopular Ones)
        # todo Ye Done Array Bara Negah Dashtan FollowingLength Gharar Nadim?
        # todo Ta Oon following Haro Avval Sort Konim Bar Asase In, Va Baad Iterate Konim?
        if len(follow_suggestions) < length:
            # todo find suggestions on the disk
            gen = [user_id for user_id in user.followings if not self.following_heap.__contains__(user_id)]
            follow_suggestions = self.iterate_for_suggestions(gen=gen, except_suggestions=except_suggestions,
                                                              length=length, follow_suggestions=follow_suggestions)
        return follow_suggestions[:length]

    # The Iterator For Suggestions
    def iterate_for_suggestions(self, gen, except_suggestions, length, user_getter=None, follow_suggestions=None):
        if user_getter is None:
            user_getter = self.get_user
        if follow_suggestions is None:
            follow_suggestions = []
        # First, We Sort Followings Based On Their Popularity
        gen = sorted(gen, key=self.get_following_length, reverse=True)
        for user_id in gen:
            if len(follow_suggestions) > length:
                break
            # Before Iteration, We Sorted ids, Based on their Popularity
            sorted_user_followings = sorted(user_getter(user_id).followings, key=self.get_following_length,
                                            reverse=True)
            follow_suggestions += \
                [suggestion_id for suggestion_id in sorted_user_followings if
                 suggestion_id not in except_suggestions and suggestion_id not in follow_suggestions]
        return follow_suggestions

    # We'll Hold A Status Block, That Holds User's Status. every line holds 750 Users (1499 Characters)
    def set_online(self, user_id: int, user_status: bool):
        if user_id >= len(self.onlines_block):
            self.onlines_block.append(user_status)
        else:
            self.onlines_block[user_id] = user_status

    def is_online(self, user_id: int):
        return False if user_id >= len(self.onlines_block) else self.onlines_block[user_id]

    def set_following_length(self, user_id: int, user_following_length: int):
        if user_id >= len(self.following_lengths_block):
            self.following_lengths_block.append(user_following_length)
        else:
            self.following_lengths_block[user_id] = user_following_length

    def get_following_length(self, user_id: str):
        return 0 if int(user_id) >= len(self.following_lengths_block) else self.following_lengths_block[int(user_id)]

    def to_block_id(self, user_id: str) -> int:
        return int((int(user_id) - 1) / self.block_size)

    def to_block_index(self, user_id: str) -> int:
        return (int(user_id) - 1) % self.block_size + 1

    # todo Update The Block Here?

    def get_block(self, block_id):
        # If Block Is In Our RAM, We'll Return It
        if self.loaded_blocks.__contains__(block_id):
            self.loaded_blocks_lrus.remove(block_id)
        else:
            # If It's Not, We'll Load It From The Disk
            self.seek_disk_for_block(block_id=block_id)
            loaded_block = self.disk_manager.read_block(disk_name=self.disk_name)
            loaded_block = Cache.to_users(block=loaded_block)
            loaded_block = [None] + loaded_block

            # We'll Replace A Block, Only When It's Popping Out of our RAM
            # todo Can We Pop With LRU Method?
            # I now do it with the LRU Method
            if len(self.loaded_blocks) == self.blocks_length - 2:
                pop_id = self.loaded_blocks_lrus.popleft()
                self.replace_block(block_id=pop_id, block=self.loaded_blocks[pop_id])
                self.loaded_blocks.pop(pop_id)
            self.loaded_blocks[block_id] = loaded_block

        self.loaded_blocks_lrus.append(block_id)
        return self.loaded_blocks[block_id]

    # Just Write The Selected Block

    def replace_block(self, block_id, block):
        block = list(map(lambda user: user.__str__(), block))
        self.seek_disk_for_block(block_id=block_id)
        self.disk_manager.write_block(disk_name=self.disk_name, data=block[1:])

    # Seek Disk For The selected block

    def seek_disk_for_block(self, block_id):
        needed_seek = self.get_delta_cursor(block_id * self.block_size)
        self.disk_manager.disk_seek(disk_name=self.disk_name, delta=needed_seek)

    # Get Cursor of the disk_manager

    def get_cursor(self):
        return self.disk_manager.disks[self.disk_name]['cursor']

    def get_delta_cursor(self, delta_line_index):
        return delta_line_index - self.get_cursor() / self.disk_manager.ENTRY_LENGTH

    @staticmethod
    def to_users(block):
        return list(map(lambda line: User.get_user(line), block))
