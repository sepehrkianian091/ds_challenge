from binary_heap import *


class FollowingHeap(MinHeap):

    def __init__(self, heap=[]):
        self.heap = heap
        self.heap_hash = {}
        if self.heap:
            for index, item in enumerate(self.heap):
                self.heap_hash[item.id] = item
                item.__setattr__('index', index)
            self.heapify()

    def extract_root(self):
        root = super().extract_root()
        self.heap_hash.pop(root.id)
        return root

    def add_element(self, element):
        super().add_element(element)
        self.heap_hash[element.id] = element

    def _swap(self, index1, index2):
        self.heap[index1].__setattr__('index', index2)
        self.heap[index2].__setattr__('index', index1)
        super()._swap(index1, index2)

    def __contains__(self, item_id):
        return self.heap_hash.__contains__(item_id)

    def get_min_following(self):
        return len(self.get_root_value().followings)

    def __getitem__(self, item_id):
        return self.heap_hash[item_id] if self.__contains__(item_id) else None

    def delete_element_at_index(self, index):
        self.heap_hash.pop(self.heap[index].id)
        super().delete_element_at_index(index)

    def replace_node(self, node):
        inner_node = self.heap_hash[node.id]
        self.heap_hash[node.id] = node
        node.__setattr__('index', inner_node.__getattribute__('index'))
        self.swim_up(node.__getattribute__('index'))
